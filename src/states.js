export default {
    "AL": {
        name: "Alabama",
        meridians: {
            "16": "Huntsville",
            "25": "Saint Stephens",
            "29": "Tallahassee",
        }
    },
    "AZ": {
        name: "Arizona",
        meridians: {
            "14": "Gila and Salt River",
        },
    },
    "AR": {
        name: "Arkansas",
        meridians: {
            "05": "Fifth Principal",
        },
    },
    "CA": {
        name: "California",
        meridians: {
            "15": "Humboldt",
            "21": "Mount Diablo",
            "27": "San Bernardino",
        }
    },
    "CO": {
        name: "Colorado",
        meridians: {
            "23": "New Mexico Principal",
            "06": "Sixth Principal",
            "31": "Ute",
        },
    },
    "FL": {
        name: "Florida",
        meridians: {
            "29": "Tallahassee",
        },
    },
    "ID": {
        name: "Idaho",
        meridians: {
            "08": "Boise",
        },
    },
    "IL": {
        name: "Illinois",
        meridians: {
            "04": "Fourth Principal",
            "02": "Second Principal",
            "03": "Third Principal",
        },
    },
    "IN": {
        name: "Indiana",
        meridians: {
            "01": "First Principal",
            "02": "Second Principal",
        },
    },
    "IA": {
        name: "Iowa",
        meridians: {
            "05": "Fifth Principal",
        },
    },
    "KS": {
        name: "Kansas",
        meridians: {
            "06": "Sixth Principal",
        }
    },
    "LA": {
        name: "Louisiana",
        meridians: {
            "18": "Louisiana",
            "24": "Saint Helena",
        },
    },
    "MI": {
        name: "Michigan",
        meridians: {
            "19": "Michigan",
        },
    },
    "MN": {
        name: "Minnesota",
        meridians: {
            "05": "Fifth Principal",
            "46": "Fourth Principal Extended",
        },
    },
    "MS": {
        name: "Mississippi",
        meridians: {
            "09": "Chickasaw",
            "10": "Choctaw",
            "16": "Huntsville",
            "25": "Saint Stephens",
            "32": "Washington",
        },
    },
    "MO": {
        name: "Missouri",
        meridians: {
            "05": "Fifth Principal",
        },
    },
    "MT": {
        name: "Montana",
        meridians: {
            "20": "Montana Principal",
        }
    },
    "NE": {
        name: "Nebraska",
        meridians: {
            "06": "Sixth Principal",
        },
    },
    "NV": {
        name: "Nevada",
        meridians: {
            "21": "Mount Diablo",
        },
    },
    "NM": {
        name: "New Mexico",
        meridians: {
            "23": "New Mexico Principal",
        }
    },
    "ND": {
        name: "North Dakota",
        meridians: {
            "05": "Fifth Principal",
        }
    },
    "OH": {
        name: "Ohio",
        meridians: {
            "01": "First Principal",
            "19": "Michigan",
        },
    },
    "OK": {
        name: "Oklahoma",
        meridians: {
            "11": "Cimarron",
            "17": "Indian",
        }
    },
    "OR": {
        name: "Oregon",
        meridians: {
            "33": "Willamette",
        },
    },
    "SD": {
        name: "South Dakota",
        meridians: {
            "07": "Black Hills",
            "05": "Fifth Principal",
            "06": "Sixth Principal",
        },
    },
    "UT": {
        name: "Utah",
        meridians: {
            "26": "Salt Lake",
            "30": "Uintah",
        },
    },
    "WA": {
        name: "Washington",
        meridians: {
            "33": "Willamette",
        },
    },
    "WI": {
        name: "Wisconsin",
        meridians: {
            "46": "Fourth Principal Extended",
        },
    },
    "WY": {
        name: "Wyoming",
        meridians: {
            "06": "Sixth Principal",
            "34": "Wind River",
        }
    }
}
