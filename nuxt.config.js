const title = 'Avris PLSS'
const description = 'A nicer interface to convert PLSS ↔ Lat & Long'
const base = 'https://plss.avris.it'
const banner = base + '/banner.png'
const colour = '#c71515';
const locale = 'en';

export default {
    target: 'static',

    head: {
        title: title,
        meta: [
            { charset: 'utf-8' },

            { hid: 'description', name: 'description', content: description },

            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'apple-mobile-web-app-title', name: 'apple-mobile-web-app-title', content: title },
            { hid: 'theme-color', name: 'theme-color', content: colour },

            { hid: 'og:type', property: 'og:type', content: 'article' },
            { hid: 'og:title', property: 'og:title', content: title },
            { hid: 'og:description', property: 'og:description', content: description },
            { hid: 'og:site_name', property: 'og:site_name', content: title },
            { hid: 'og:image', property: 'og:image', content: banner },

            { hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' },
            { hid: 'twitter:title', property: 'twitter:title', content: title },
            { hid: 'twitter:description', property: 'twitter:description', content: description },
            { hid: 'twitter:site', property: 'twitter:site', content: base },
            { hid: 'twitter:image', property: 'twitter:image', content: banner },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: 'data:image/svg+xml,<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 100 100\'><text y=\'0.9em\' font-size=\'90\'>🌎</text></svg>' }
        ]
    },
    env: {
        APPLE_MAP_TOKEN: process.env.APPLE_MAP_TOKEN,
    },
    components: true,
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        'vue-plausible',
    ],
    axios: {},
    pwa: {
        manifest: {
            name: title,
            short_name: title,
            description: description,
            background_color: '#ffffff',
            theme_color: colour,
            lang: locale,
        }
    },
    plausible: {
        domain: 'plss.avris.it',
        apiHost: 'https://plausible.avris.it',
    },
}
