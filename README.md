# PLSS

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## author

- andrea vos
- [avris.it](https://avris.it)
- license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
